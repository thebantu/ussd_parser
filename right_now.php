<?php

require_once

use

/* The main USSD Request Class */
class
    protected $service_code;
    protected $ussd_string;
    protected $session_id;
    protected $msisdn;
    protected $input;
    protected $operator;
    protected $level;
    protected $shortcut;
    protected $menu;
    protected $logger;
    protected $config;
    protected $valid_methods;
    protected $acceptable_params;
    protected $default_session_end_message;
    protected $ussd_string_text;
    protected $menu_footer;
    private $session_data = false;
    private $cache_data = [];
    private $cache = null;
    private $db = null;


    public function __construct () {
        $this->initialize_ussd_settings();
    }

    private function set_ussd_params ( $data ) {
        $this->service_code = $data['service_code'];
        $this->msisdn = $data['msisdn'];
        $this->ussd_string = urldecode($data['ussd_string']);
        $this->session_id = $data['session_id'];
        $this->add_to_cache_data('session_id', $this->session_id);
    }

    //NOTE: Cache key example: 699#254712171204
    private function get_session_data () {
        $rdata = [];
        $key = $this->service_code . "#" . $this->msisdn;
        $cdata = $this->cache->retrieve_from_cache($key);
        if ( $cdata ) 
           $rdata = array_merge($rdata, json_decode($cdata['results'], true));
        return $rdata;
    }

    private function clean_ussd_string () {
        if ( empty($this->shortcut) )
            return $this->ussd_string;
        $reg = '/' . addcslashes($this->shortcut, '*') . '\*?/';
        $subject = $this->service_code . '*' . $this->ussd_string;

        $rvalue = preg_replace($reg, '', $subject);
        return $rvalue;
    }

    private function ussd_string_level () {
        $ussd_string = $this->clean_ussd_string();
        if ( empty($ussd_string) )
            return 0;
        return count(
                split('\*', $ussd_string)
        );
    }
        
    private function get_level () {
        /* This session is NOT fresh */
        $ussd_string_level = $this->ussd_string_level();
        if (isset($this->session_data['level']) and
            $this->session_data['level'] < $ussd_string_level) {
            $level = ++$this->session_data['level'];
        }
        /* Level is the same as before */
        else if ($this->session_data['level'] == $ussd_string_level) {
            $level = $ussd_string_level;
        }
        /* This session IS fresh */
        else {
            $level = 0;
        }
        $this->add_to_cache_data('level', $level);
        return $level;
    }

    /* When input is false it is either not there at all
     * it was a shortcut's first dial ie the contents of
     * ussd_string make up the shortcut.
     */ 
    public function get_input () {
        if ( $this->level > 0 )
            return preg_replace($this->get_regex('last_input'), "$1", $this->ussd_string);
        else
            return false;
    }

    public function get_shortcut () {
        /* This session is NOT fresh */ 
        if (isset($this->session_data['shortcut'])) {
            $shortcut = $this->session_data['shortcut'];
        }
        /* This session is fresh and ussd_string is NOT empty */
        else if ( $this->level == 0 and ! empty($this->ussd_string) ) {
            $shortcut = $this->service_code . "*" . $this->ussd_string;
        }
        /* No Shortcut was found */
        else
            $shortcut = false;
        $this->add_to_cache_data('shortcut', $shortcut);
        return $shortcut;
    }

    public function get_operator () {
        preg_match_all($this->get_regex('op_code'), $this->msisdn, $results);
        $op_code = isset($results[1]) ?: 2;
        switch ($op_code) {
            case 0: case 1: case 2: case 9:
                $operator = "safaricom";
                break;
            case 5: case 3: case 8:
                $operator = "airtel";
                break;
            case 7:
                $operator = "orange";
                break;
            default:
                $operator = "safaricom";
        }
        return $operator;
    }

    public function get_regex( $key ) {
        /* Add new regexes to this $regexes array
         * when calling it the key should correspond to
         * the array's key
         */
        $regexes = [
            /* Valid json content */
            'json' => '!application/json!',
            /* Valid xml content */
            'xml' => '!application/xml!',
            /* Valid plain text content */
            'text' => '!text/plain!',
            /* Valid html content */
            'html' => '!text/html!',
            /* HTTP POST */
            'post' => '!application/x-www-form-urlencoded!',
            /* Valid Kenyan MSISDN.
             * If ever this app is to be used by ussd requests
             * a proper function for identifying a valid country's
             * MSISDN should be implemented.
             */
            'mobile_num' => '/(?:0|254)?(7\d{8})/',
            /* Find digits only. */
            'digits' => '/^\d+$/',
            /* Get special characters inside a field name. */
            'field_special_chars' => '/[.]/',
            /* Positive match test for content type check */
            'content_type_positive_test' => '/7\~/',
            /* Look for error string */
            'error_str' => '/^(ERROR|Notice)/i',
            /* Get Op code */
            'op_code' => '/^(?:254|0)?7(.)/',
            /* Get last input */
            'last_input' => '/.*?\*?([^*]+)$/',
            /* Get star */
            'star' => '/\*/',

        ];
        return $regexes[$key];
    }

    private function match_content_type ( $content_type ) {
        $valid_content_types = [ 'post', 'json', 'text', 'xml' ];

        /* 7 - Success
         * 6 - Failure
         * On match append the content type to 7
         */
        $check_content_type = function ( $test ) use ( $content_type ) {
            return (preg_match($this->get_regex( $test ), $content_type) ? "7~" . $test : 6);
        };
    
        /* Check if a valid content type was found */
        $positive_match = function ( $res ) {
            return preg_match($this->get_regex('content_type_positive_test'), $res);
        };

        $results = array_map( $check_content_type, $valid_content_types );
        $matches = array_values(array_filter( $results, $positive_match ));
        return (count( $matches ) > 0) ? substr( $matches[0],2 ) : false;
    }
        
    /* Get the content type of the request body */
    private function content_type () {
        return $this->match_content_type( $_SERVER['CONTENT_TYPE']);
    }

    /* Read the request body for the POST */
    private function read_data () {
        return file_get_contents('php://input');
    }

    /* Check the parameters of the submitted data 
     * against an acceptable list of params
     */
    private function validate_ussd_params ( $data ) {
        return !count(array_diff(array_keys($data), $this->acceptable_params));
    }

    private function valid_params () {
        return split(',', $this->config['settings']['acceptable_params']);
    }

    private function initialize_ussd_settings () {
        /* Read App Config */
        $this->config = Config::get_config();
        $this->valid_methods = $this->valid_methods();
        $this->acceptable_params = $this->valid_params();

        /* Bootstrap some resources */
        $this->cache = new MemcacheLib(200);
        $this->db = new Db;
        $this->logger = new Logger($this->config['logging']['logfile']);

        /* Bootstrap USSD */
        $this->set_ussd_params($this->receive_data());
        $this->operator = $this->get_operator();
        $this->session_data = $this->get_session_data(); 
        $this->shortcut = $this->get_shortcut();
        $this->level = $this->get_level();
        $this->input = $this->get_input();
        $this->ussd_string_text = $this->get_ussd_string_from_menu();
        $this->persist_cache_data();
    }
    
    private function valid_methods () {
        return split(',', $this->config['settings']['valid_methods']);
    }

    /* Only POST is valid */
    private function is_valid_method () {
        return ( in_array($_SERVER['REQUEST_METHOD'], 
            array_map('strtoupper', $this->valid_methods()))
        );
    }

    /* Parse received data */
    private function receive_data () {
        if ($this->is_valid_method()) {
            $type = $this->content_type();
            $data = $this->read_data();
        }
        else
        return false;
        switch ( $type ) {
          case "json":
              $rdata = json_decode($data);
              break;
          case "xml":
              $rdata = parse_xml($data);
              break;
          case "post":
              parse_str($data, $rdata);
              break;
        }
        if ($this->validate_ussd_params($rdata))
            return $rdata;
        else
            return false;
    }

    /**
     * For Text menu to work -- We create a parent menu with menu_category TEXT
     * Have all the children to this parent as TEXT Menus as well
     */
    public function get_ussd_string_from_menu () {
        /* Get the current node from the tree */
        $current_node = $this->get_current_node();

        /* Fetch the current menu and header. */
        $this->set_menu();
        $current_menu =  $this->get_current_menu($current_node);
        PHP_EOL;

        /* Get the current node type from the tree
         * and save it to cache_data.
         */
        $current_node_type = $current_menu['menu_category'];
        if ( $current_node_type == 'TEXT' )
            $header = $this->session_data['previous_header'];
        else
            $header = $current_menu['menu_name'];

        $this->add_to_cache_data('previous_header', $header);
        $this->add_to_cache_data('last_node_type', $current_node_type);

        /* Get the endpoint if at all it is set */
        if ( $current_menu['end_point'] )
            $endpoint = $current_menu['end_point'];
        else
            $endpoint = null;

        
        /* Current node is a navigation node */
        if( $current_node_type != 'TEXT' ){
            $menu_text = $this->get_menu_children($current_menu['menu']);
        }

        /* Take care of nodes with endpoints */
        else if ( $endpoint ) {
            $menu_text = $this->call_endpoint($endpoint);
        }

        /* Current node is a text node */
        else {
            PHP_EOL;
            $menu_text = $this->get_text_menu_text($current_menu);
        }
        /* Add End to string To End the session on the user's phone
                $this->set_menu_footer( 'end' );
                $menu_text = $this->append_footer($menu_text);
              ---------------
         */

        if( !$menu_text ) {

           /* We have exhausted text menus ... someone load the next thing
            * At this point you should end the session with a session end
            * message.
            */

            $menu_text = $this->default_session_end_message;
        }
        $menu_text = $header . PHP_EOL . $menu_text;
        return $menu_text;

    }
    
    private function set_menu_footer ( $key ) {
        $footers = [
                 'con' => 'CON',
                 'end' => 'END',
        ];
        $this->menu_footer = $footers[$key];
    }

    private function append_footer ( $menu_text ) {
        $fmt = '%s%s';
        return sprintf($fmt, $menu_text, $this->menu_footer);
    }
        

    /* Prints the string to be displayed on the user's phone */
    public function print_ussd_string () {
    }

    private function get_shortcut_string () {
        $reg = '/' . $this->service_code . '\*/';
        return preg_replace($reg, '', $this->shortcut);
    }
    /* Menus are cached with the key defined as follows:
     * service_code[*shortcut]
     * Note: The section in square brackets is optional.
     */
    public function set_menu () {
        if ($this->shortcut) {
            $key = $this->service_code . '*' . $this->get_shortcut_string();
        }
        else {
            $key = $this->service_code;
        }

        $cache_data = $this->cache->retrieve_from_cache($key);
        //print print_r($cache_data, true) . "< = Cache'd menu" . PHP_EOL;

        /* The Cache'd menu */
        $menu = json_decode($cache_data['results'], true);

        /* If the menu was not found in cache fetch it from the DB
         * and cache it.
         */
        if (!$menu) {
            if ($this->shortcut)
                $query_shortcut = '*' .  $this->shortcut . '#';
            else 
                $query_shortcut = false;
            $menu = $this->db->get_menu($this->service_code, $query_shortcut);
            /* Cache menu for a day */
            $this->cache->save_to_cache($key, json_encode($menu), 60*60*24);
        }
        $this->menu = $menu;
    }

   //NOTE: Add prev_text_menu_node to cache <= Done
   //Prev node is the index of the previous menu displayed
    private function get_text_menu_text($menu){
        $prev_text_menu_node = isset($this->session_data['prev_text_menu_node']) ?
                    /* True */
                    $this->session_data['prev_text_menu_node'] :
                    /* False */
                    false;  
        /* Check if there was a previous node */
        if ( $prev_text_menu_node ) {
            /* Check if we're on the last text node */
            if ( ( $prev_text_menu_node + 1 ) == count( $menu ) ) {
                $text_menu = false;
            }
            /* There are still some nodes to go. */
            else {
                $text_menu = $menu[++$prev_text_menu_node]['menu_name'];
            }
            $cache_value = $prev_text_menu_node;
        }
        /* Return this node */
        else {
            $text_menu = $menu['menu_name'];    
            $cache_value = 0;
        }
        $this->add_to_cache_data('prev_text_menu_node', $cache_value);
        return $text_menu;
    }
    
    private function persist_cache_data () {
        $key = $this->service_code . '#' . $this->msisdn;
        $data = $this->cache_data;
        $this->cache->save_to_cache($key, json_encode($data), 200);
    }

    /* Appends the details supplied to the data to be cache'd */
    private function add_to_cache_data ( $key, $value ) {
        $this->cache_data = array_merge( 
                    $this->cache_data,
                    [ $key => $value ]
                );
    }

   public function get_menu_node($menu, $node, $start=false){
        $node_arr = split('\.', $node);
        $new_node = substr($node, strpos($node, '.'), strlen($node));
        $menu = $start? $menu[$node_arr[0]]:$menu['menu'][$node_arr[0]];

        if(strlen($new_node) > 0){
            return get_menu_node($menu[$node_arr[0]], $new_node);
        }
        return $menu;
    }

    private function get_current_menu ($node){
        $menu_copy = $this->menu;
        $node_arr = split('\.', $node);
        $traverse = function ($val, $key) use (&$menu_copy) {
            if ($key == 0) 
                $menu_copy = $menu_copy[$val];
            else
                $menu_copy = $menu_copy['menu'][$val];
        };

        /* Traverse menu */
        array_walk($node_arr, $traverse);
        return $menu_copy;
    }

    /* Returns an array with the menu at the current level */
    private function get_current_menu_legacy ($node){
        $node_arr = split('\.', $node);
        $traverse = function ($ca, $v) use ($node_arr) {
            if (count($node_arr) % 2) 
                return $ca[$v];
            else
                return $ca['menu'][$v];
        };

        $this->set_menu();
        /* Traverse menu */
        $last_menu =  array_reduce($node_arr, $traverse, $this->menu);
        return $last_menu;
    }

    private function get_current_node_from_ussd_string () {
        $ussd_str = $this->clean_ussd_string();
        $us = split('\*', $ussd_str);
        array_walk($us, function ($v, $k) use (&$us) {
            $us[$k] = $v - 1;
        });

        return '0.' . join('.', $us);
    }

        
    private function node_to_array ( $node ) {
        return split('\.', $node);
    }
    /* Retuns a string representation of the
     * traversal instructions to be applied
     * to obtain the current menu.
     */
    private function get_current_node() {
        $last_node = isset($this->session_data['last_node']) ?
            $this->session_data['last_node'] : false;
        $last_node_type =  isset($this->session_data['last_node_type']) ?
            $this->session_data['last_node_type'] : false;

        /* This is NOT a fresh session and the
         * node selected is a navigation
         * node. ie the selection is NOT an endpoint
         * Input is decremented since php arrays
         * are zero indexed.
         */
        if ( 
            /* $last_node is set */
            ($last_node != NULL)
            &&
            ($last_node_type != 'TEXT')
           ) {
            $current_node = $this->get_current_node_from_ussd_string();
        }

        /* Session is NOT fresh and
         * selected node is a TEXT node
         */
         else if (
                   /* $last_node is set */
                   ($last_node != NULL)
                   &&
                   /* $last_node_type is TEXT */
                   ($last_node_type == 'TEXT')
                 ) {
              $current_node = $last_node;
         }

        /* Session is fresh */
        else {
            $current_node = '0';
        }
        $this->add_to_cache_data('last_node', $current_node);
        return $current_node;
        
    }
    
    /* Returns text comprising of the names of the
     * menu item names.
     */
    public function get_menu_children ( $menu ) {
        $index=0;
        $get_menu_text = function($cur, $value) use (&$index) {
            return $cur . ++$index . '. ' . $value['menu_name'] . PHP_EOL;
        };

        $menu_text = array_reduce($menu, $get_menu_text);
            
        return $menu_text;
    }
    public function call_endpoint ( $endpoint, $data = false, $headers = false ) {
        if ( $headers ) {
            $request = new Url($endpoint, true);
        }
        else {
            $request = new Url($endpoint);
        }
        if ( $data ) {
            $request->setPost($data);
        }
        $request->callUrl();
        return $request;
    }
}


/* Routines
 * · __construct
 * · set_ussd_params
 * · get_session_data
 * · clean_ussd_string
 * · ussd_string_level
 * · get_level
 * · get_input
 * · get_shortcut
 * · get_operator
 * · get_regex
 * · match_content_type
 * · content_type
 * · read_data
 * · validate_ussd_params
 * · valid_params
 * · initialize_ussd_settings
 * · valid_methods
 * · is_valid_method
 * · receive_data
 * · get_ussd_string_from_menu
 * · set_menu_footer
 * · append_footer
 * · print_ussd_string
 * · get_shortcut_string
 * · set_menu
 * · get_text_menu_text
 * · persist_cache_data
 * · add_to_cache_data
 * · get_menu_node
 * · get_current_menu
 * · get_current_menu_legacy
 * · get_current_node_from_ussd_string
 * · node_to_array
 * · get_current_node
 * · get_menu_children
 * · call_endpoint
 */


 /* Test Class */

$ussd = new Request;
