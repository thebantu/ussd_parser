<?php

define('ROOT_DIR', '/var/www/html/ussd_request_parser/lib');

$classes_dir = array (
    /* Lib Dir */
    ROOT_DIR . '/',
    /* Endpoint Classes */
    ROOT_DIR . '/endpoints/',
    /* Interfaces Dir */
    ROOT_DIR . '/interfaces/',
);

function __autoload($class_name) {
    /* Handle classes with name spaces */
    $class_parts = explode( '\\', $class_name );
    $class = end( $class_parts );
    global $classes_dir;
    foreach ($classes_dir as $directory) {
        $file = $directory . $class . '.php';
        if (file_exists( $file )) {
            require_once ( $file );
            return;
        }
    }
    print "Could not find classname" . PHP_EOL;
}
