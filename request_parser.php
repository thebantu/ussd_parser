<?php
namespace Ussd;

require_once('.autoload/autoload.php');

/* 
 * ---------------------------------------------------------------------------
 *  This is the ussd request parser class.
 *  It's main responsibility when it
 *  receives a request is to:
 *       = Parse the request.
 *       = Retrieve a relevant menu depending on the request.
 *       = Traverse the menu accordingly.
 *       = Return processed menu text.
 *
 *  USAGE:
 *       $menu_text = new Request;
 *       print $menu_text;
 *  --------------------------------------------------------------------------
 *  Acknowledgements:
         - Reuben: USSD Tree Structure, Db Class V1, miscellaneous
           contributions.
         - Tapis: Handling double safaricom requests for USSD shortcut dials.
 *  --------------------------------------------------------------------------
 *  Read well before editing :)
 *
 *  |-_-|
 * ---------------------------------------------------------------------------
 */


use \Endpoint as Endpoint;
use Utils\Configuration\Config;

/* The main USSD Request Class */
class Request {
    /* Moderately visible data */
    protected $service_code;
    protected $ussd_string;
    protected $endpoint_ussd_string;
    protected $session_id;
    protected $msisdn;
    protected $input;
    protected $operator;
    protected $level;
    protected $shortcut;
    protected $menu;
    protected $logger;
    protected $config;
    protected $valid_methods;
    protected $acceptable_params;
    protected $default_session_end_message;
    protected $ussd_string_text = false;
    protected $menu_prefix;
    protected $is_last_menu = 0;
    protected $is_content_menu = 0;
    protected $db_request_id;

    /* Hidden data */
    private $session_data = false;
    private $cache_data = [];
    private $cache = null;
    private $db = null;


    /* Bootstrap the application */
    public function __construct () {
        $this->initialize_ussd_settings();
    }

    private function initialize_ussd_settings () {
        /* Read App Config */
        $this->config = Config::get_config();
        $this->logger = new Logger($this->config['logging']['logfile']);
        $this->valid_methods = $this->valid_methods();
        $this->acceptable_params = $this->valid_params();

        /* Bootstrap some resources */
        $this->cache = new Cache(200);
        $this->db = new Db;

        /* Bootstrap USSD */
        $this->set_ussd_params($this->receive_data());
        $this->session_data = $this->get_session_data(); 
        $this->logger->logInfo('Previous SESSION DATA => ' .
            print_r($this->session_data, true)
        );
        /* Handle the double requests that are dispatched by Safaricom
         * for initial ussd shortcut dials. 
         */
        if ( array_key_exists('raw_ussd_string', $this->session_data ) ) { 
            if ( $this->session_data['raw_ussd_string'] == 
                 $this->ussd_string
               )
            {
                $this->ussd_string_text =
                    $this->session_data['last_ussd_text'];
                return;
            }
        }
        else { 
            $this->add_to_cache_data('raw_ussd_string', $this->ussd_string);
        }
        $this->level = $this->get_level();
        $this->logger->logInfo('FOUND LEVEL => '. $this->level);
        $this->shortcut = $this->get_shortcut();
        $this->logger->logInfo('Shortcut => '. $this->shortcut);
        $this->input = $this->get_input();
        $this->logger->logInfo('INPUT STRING => ' .$this->input);
        $this->db_request_id = $this->persist_ussd_request();
        $this->add_to_cache_data('db_request_id', $this->db_request_id);
        $this->ussd_string_text = $this->get_ussd_string_from_menu();
        $this->logger->logInfo('USSD STRING TEXT => ' .
            $this->ussd_string_text
        );
        $this->add_to_cache_data('last_ussd_text', $this->ussd_string_text);
        $this->set_endpoint_ussd_str();
        $this->persist_cache_data();
        $this->logger->logInfo('Current SESSION DATA => ' .
            print_r($this->session_data, true)
        );
    }
    
    /* Fetch some USSD Params */
    private function set_ussd_params ( $data ) {
        $this->service_code = $this->get_service_code($data['service_code']);
        $this->msisdn = $data['msisdn'];
        $this->ussd_string = urldecode($data['ussd_string']);
        $this->operator = $this->get_operator();
        $this->endpoint_ussd_string = 'None';
        $this->session_id = $data['session_id'];
        $this->default_session_end_message =
            $this->config['defaults']['default_message'];

        /* Save some things of interest in the cache */
        $this->add_to_cache_data('ep_ussd_string', $this->endpoint_ussd_string);
        $this->add_to_cache_data('session_id', $this->session_id);

        /* Record some stuff in the log just in case of trouble */
        $this->logger->logInfo('USSD PARAMS: ' .
            'Service Code => ' . $this->service_code . ', MSISDN => ' .
            $this->msisdn . ' USSD_STING => ' .
            $this->ussd_string . ' SESSION_ID => ' .
            $this->session_id 
        );
    }

    //NOTE: Cache key example: 699#254712171204
    private function get_session_data () {
        $rdata = [];
        $key = $this->service_code . "#" . $this->msisdn;
        $cdata = $this->cache->retrieve_from_cache($key);
        if ( $cdata['results'] ) 
           $rdata = array_merge($rdata, json_decode($cdata['results'], 1));
        return $rdata;
    }

    /*
     * Package ussd data for logging into the 
     * ussd_request_table
     */
    private function pack_request_data () {
        return [
                'msisdn' => $this->msisdn,
                'operator' => $this->operator,
                'session_id' => $this->session_id,
                'service_code' => $this->service_code,
                'shortcut' => $this->shortcut,
                'ussd_string' => $this->ussd_string,
                'narration' => $this->session_data['last_ussd_text'],
                'parameters' => $this->input,
                'encoded_session' => sha1(time()),
                'created' => date('Y-m-d H:i:s'),
               ];
    }

    /*
     * Check if the current menu is a content menu.
     */
    private function is_content_menu ( $menu_id ) {
        return ($this->db->check_content_menu( $menu_id ));
    }
   
    /*
     * Go on and save that ussd request to the database
     */
    private function persist_ussd_request () {
        $data = $this->pack_request_data();
        return $this->db->log_ussd_request( $data );
    }

    private function get_service_code ($service_code) {
        $regex = '/[*#]/';
        return preg_replace($regex, '', $service_code);
    }

    private function set_endpoint_ussd_str () {
        switch ( $this->endpoint_ussd_string ) {
            case 'None':
                $this->endpoint_ussd_string = 'Start';
                break;
            case 'Start':
                $this->endpoint_ussd_string = $this->input;
                break;
            default:
                $this->endpoint_ussd_string .= '*' . $this->input;
        }
    }

    private function clean_ussd_string () {
        /* If shortcut is empty then this is the first dial
         * so return the contents of the ussd_string param
         * if they exist
         */
        if ( empty($this->shortcut) )
            return $this->ussd_string;
        $reg = '/' . addcslashes($this->shortcut, '*') . '\*?/';
        $subject = $this->service_code . '*' . $this->ussd_string;

        $rvalue = preg_replace($reg, '', $subject);
        return $rvalue;
    }

    private function ussd_string_level () {
        $ussd_string = $this->clean_ussd_string();
        if ( empty($ussd_string) )
            return 0;
        return count(
                split('\*', $ussd_string)
        );
    }
        
    private function get_level () {
        /* This session is NOT fresh */
        $ussd_string_level = $this->ussd_string_level();
        /* This session IS fresh */
        if ( !$this->session_data ) {
            $level = 0;
        }
        else if (
                isset($this->session_data['level'])
                and
                $this->session_data['level'] < $ussd_string_level
            ) {
            $level = ++$this->session_data['level'];
        }
        /* Level is the same as before this is the case for text nodes */
        else if ($this->session_data['level'] == $ussd_string_level) {
            $level = $ussd_string_level;
        }
        else {
            $level = 0;
        }
        $this->add_to_cache_data('level', $level);
        return $level;
    }

    /* When input is false it is either not there at all
     * it was a shortcut's first dial ie the contents of
     * ussd_string make up the shortcut.
     */ 
    public function get_input () {
        if ( $this->level > 0 )
            return preg_replace($this->get_regex('last_input'), "$1", $this->ussd_string);
        else
            return false;
    }

    public function get_shortcut () {
        /* This session is NOT fresh */ 
        if (isset($this->session_data['shortcut'])) {
            $shortcut = $this->session_data['shortcut'];
        }
        /* This session is fresh and ussd_string is NOT empty */
        else if ( $this->level == 0 and ! empty($this->ussd_string) ) {
            $shortcut = $this->service_code . "*" . $this->ussd_string;
        }
        /* No Shortcut was found */
        else
            $shortcut = false;
        $this->add_to_cache_data('shortcut', $shortcut);
        return $shortcut;
    }

    public function get_operator () {
        preg_match_all($this->get_regex('op_code'), $this->msisdn, $results);
        $op_code = isset($results[1]) ?: 2;
        switch ($op_code) {
            case 0: case 1: case 2: case 4: case 9:
                $operator = "safaricom";
                break;
            case 5: case 3: case 8:
                $operator = "airtel";
                break;
            case 7:
                $operator = "orange";
                break;
            default:
                $operator = "safaricom";
        }
        return $operator;
    }

    public function get_regex( $key ) {
        /* Add new regexes to this $regexes array
         * when calling it the key should correspond to
         * the array's key
         */
        $regexes = [
            /* Valid json content */
            'json' => '!application/json!',
            /* Valid xml content */
            'xml' => '!application/xml!',
            /* Valid plain text content */
            'text' => '!text/plain!',
            /* Valid html content */
            'html' => '!text/html!',
            /* HTTP POST */
            'post' => '!application/x-www-form-urlencoded!',
            /* Valid Kenyan MSISDN.
             * If ever this app is to be used by ussd requests
             * a proper function for identifying a valid country's
             * MSISDN should be implemented.
             */
            'mobile_num' => '/(?:0|254)?(7\d{8})/',
            /* Find digits only. */
            'digits' => '/^\d+$/',
            /* Get special characters inside a field name. */
            'field_special_chars' => '/[.]/',
            /* Positive match test for content type check */
            'content_type_positive_test' => '/7\~/',
            /* Look for error string */
            'error_str' => '/^(ERROR|Notice)/i',
            /* Get Op code */
            'op_code' => '/^(?:254|0)?7(.)/',
            /* Get last input */
            'last_input' => '/.*?\*?([^*]+)$/',
            /* Get star */
            'star' => '/\*/',

        ];
        return $regexes[$key];
    }

    private function match_content_type ( $content_type ) {
        $valid_content_types = [ 'post', 'json', 'text', 'xml' ];

        /* 7 - Success
         * 6 - Failure
         * On match append the content type to 7
         */
        $check_content_type = function ( $test ) use ( $content_type ) {
            return (preg_match($this->get_regex( $test ), $content_type) ? "7~" . $test : 6);
        };
    
        /* Check if a valid content type was found */
        $positive_match = function ( $res ) {
            return preg_match($this->get_regex('content_type_positive_test'), $res);
        };

        $results = array_map( $check_content_type, $valid_content_types );
        $matches = array_values(array_filter( $results, $positive_match ));
        return (count( $matches ) > 0) ? substr( $matches[0],2 ) : false;
    }
        
    /* Get the content type of the request body */
    private function content_type () {
        if(isset($_SERVER['CONTENT_TYPE'])){
            return $this->match_content_type( $_SERVER['CONTENT_TYPE']);
        }
        return 'text';
    }

    /* Read the request body for the POST */
    private function read_data ($method='GET') {
        if($_SERVER['REQUEST_METHOD'] == 'GET'){
            $this->logger->logInfo('Returning the the query string => ' .
            $_SERVER["QUERY_STRING"]);
            return $_SERVER["QUERY_STRING"];
        }
        $this->logger->logInfo('Found other request method ' .
        $_SERVER['REQUEST_METHOD']);
        return file_get_contents('php://input');
    }

    /* Check the parameters of the submitted data 
     * against an acceptable list of params
     */
    private function validate_ussd_params ( $data ) {
        $this->logger->logInfo( "Inside (" . __FUNCTION__ . ") " . print_r($data, true));
        if(empty($data)){
           return false; 
        }
        $vp = [];
        $keys = array_keys($data);
        array_walk($keys, 
            function (&$v, $k) { 
                $v = strtoupper($v); 
            }
        );
        array_walk($this->acceptable_params, 
            function ($v, $k) use ( &$vp ) { 
                $vp[] = strtoupper($v); 
            }
        );

        return !count( array_diff($keys, $vp) );
    }

    private function valid_params () {
        $this->logger->logInfo('Configs settings =>' . $this->config['settings']['acceptable_params']);
        $vparams =  explode(',', $this->config['settings']['acceptable_params']);
        $this->logger->logInfo('Valid params from config =>' .
        print_r($vparams, true));
        return $vparams;
    }

    private function valid_methods () {
        return split(',', $this->config['settings']['valid_methods']);
    }

    /* Only POST is valid */
    private function is_valid_method () {
        $this->logger->logInfo('RECEIVED METHOD => '.
        print_r($_SERVER['REQUEST_METHOD'], true));

        return ( in_array($_SERVER['REQUEST_METHOD'], 
            array_map('strtoupper', $this->valid_methods()))
        );
    }

    /* Parse received data */
    private function receive_data () {
        if ($this->is_valid_method()) {
            $type = $this->content_type();
            $data = $this->read_data();
            $this->logger->logInfo('READ DATA RETURNED =>' . print_r($data,
            true));
        }
        else{
            $this->logger->logInfo('Receive Data found invalaid data ');
            return false;
        }
        switch ( $type ) {
          case "json":
              $this->logger->logInfo('request type json calling decode');
              $rdata = json_decode($data);
              break;
          case "xml":
              $this->logger->logInfo('request type xml calling parse xml');
              $rdata = parse_xml($data);
              break;
          case "post":
              $this->logger->logInfo('request type post calling parser_str');
              parse_str(urldecode($data), $rdata);
              break;
          default:
              $this->logger->logInfo('request other calling parser_str');
              parse_str(urldecode($data), $rdata); 
              break;
        }
        $this->logger->logInfo('Decoded URL PARAMS => '. print_r($rdata,
        true));
        if ( $this->validate_ussd_params($rdata) ) {
            return $rdata;
        }else{
            return false;
        }
    }

    /**
     * For The Text menu to work as is it should --
     * We create a parent menu with menu_category TEXT
     * Have all the children to this parent as TEXT Menus as well
     */
    public function get_ussd_string_from_menu () {
        /* Get the current node from the tree */
        $current_node = $this->get_current_node();


        /* Fetch the current menu and header. */
        $this->set_menu();
        $this->logger->logInfo('The menu: ' . print_r($this->menu, 1));
        if ( !$this->menu ) 
            return $this->default_session_end_message;

        $current_menu =  $this->get_current_menu($current_node);

        /* Get the current node type from the tree
         * and save it to cache_data.
         */
        $current_node_type = $current_menu['menu_category'];
        $this->logger->logInfo('Node type found: ' . $current_node_type );
        if ( $current_node_type == 'TEXT' and
            /* Look for the the menu header in the cache */
            array_key_exists('previous_header', $this->session_data)
           )

            /* Set $header if you find it */
            $header = $this->session_data['previous_header'];
        else
            /* Set $header to the menu name 
             * of the current menu when not found
             */
            $header = $current_menu['menu_name'];

        /* Persist the menu header to Cache */
        $this->add_to_cache_data('previous_header', $header);

        /* Persist the node type to Cache */
        $this->add_to_cache_data('last_node_type', $current_node_type);

        /* Get the endpoint if at all it is set */
        if ( $current_menu['end_point'] )
            $endpoint = $current_menu['end_point'];
        else
            $endpoint = null;

        /* Check if the current menu is a content menu */
        if ( $current_node_type == 'TEXT' ) {
            $this->logger->logInfo('Evaluating if this is a content menu node.');
            if (    $this->is_content_menu(
                            $current_menu['ussd_menu_id']
                    )
               )
                $this->is_content_menu = 1;
            else
                $this->is_content_menu = 0;
 
            /* Persist the is_content_menu flag to the cache or increment it by 1 */
            if ( array_key_exists('is_content_menu', $this->session_data ) ) {
                /* Increment by 1 if exists */
                $aggr = (
                            $this->is_content_menu +
                            $this->session_data['is_content_menu']
                        );
                $this->logger->logInfo('Incrementing cache value to ' .
                                        $aggr . 'since is_content_menu' .
                                        ' was already set'
                                      );
                $this->add_to_cache_data('is_content_menu', $aggr );
            }
            else {
                $this->logger->logInfo('Caching is_content_menu value. (' .
                                        $this->is_content_menu . ')'
                                      );
                /* Create cache entry, when it doesn't exist. */
                $this->add_to_cache_data('is_content_menu', $this->is_content_menu);
            }
        }

        /* Current node is a navigation node */
        if( $current_node_type != 'TEXT' ){
            $menu_text = $this->get_menu_children($current_menu['menu']);
        }

        /* Take care of content menus yet to be presented with the payment
         * options. The assumption is content menu's imply that they are 
         * text nodes ie not navigation nodes.
         */
        else if ( 
                $this->is_content_menu == 1 and
                !isset($this->session_data['is_content_menu'])
            ) {
            $menu_text = "Buy this song via:\n1. Airtime\n2. MPESA";

        }

        /* Take care of content menus whose selected payment option is
         * airtime.
         */
        else if ( 
                $this->is_content_menu > 0 and
                isset($this->session_data['is_content_menu']) and
                /* Check if the saved value of is_content_menu is 0 */
                $this->session_data['is_content_menu'] == 1
            ) {
            $content_details = $this->db->retreive_content_details(
                                            $current_menu['ussd_menu_id']
                               );
            if ( $this->input == 1 ) {
                $menu_text = "Select YES on next menu to get %s by " .
                             "%s to your phone";
                $menu_text = vsprintf( $menu_text, $content_details);
            }
            else if ( $this->input == 2 ) {
                $menu_text = "Enter your bonga pin in the next menu to get" .
                             " %s by %s to your phone. Don't have Bonga " .
                             "Pin? Dial *126*5#";
                $menu_text = vsprintf( $menu_text, $content_details);
            }

            /* Time to invoke the content endpoint */
            
            /* Get service data */
            $service_data = $this->package_data(
                                $current_menu['ussd_menu_id'],
                                $current_menu['end_point']
                            );
            $ep = new Endpoint(
                               $service_data,
                               [ 
                                   'endpoint' => $this->config['settings']['content_endpoint']
                               ]
                              );
            /* Make this the last menu displayed */
            $this->is_last_menu = 1;

            $menu_text = '';
            return $menu_text;
                             

        }

        /* Take care of nodes with endpoints */
        else if ( $endpoint and $this->is_last_menu == 0) {
            $service_data = $this->package_data(
                                $current_menu['ussd_menu_id'],
                                $current_menu['end_point']
                            );
            /* Legacy endpoint logic
             *  $service_data = (empty($service_data)) ? $service_data : false;
             *  $menu_text = $this->call_endpoint( $endpoint, $service_data );
             */
            $ep = new Endpoint($service_data);
            $menu_text = $ep->get_ussd_menu();
            return $menu_text;

        } 
        /* Current node is a text node */
        else {
            $menu_text = $this->get_text_menu_text($current_menu['menu']);
        }

        /* 
         * ---
         * You should ALWAYS end up in this following block if you have run
         * out of nodes to traverse.
         * ---
         */
        if( !$menu_text or strlen($menu_text) < 1 ) {

           $service_details = $this->db->retreive_base_service_details(
                       $current_menu['service_id']
                   );
           $patterns = [ '/\{service\}/', '/\{shortcut\}/' ];
           $menu_text = preg_replace( $patterns, 
                                      $service_details,
                                      $this->default_session_end_message
                        );
            $this->logger->logInfo("Before EP: This is the menu text => " . $menu_text);
           /* We have exhausted text menus ... someone load the next thing
            * At this point you should end the session with a session end
            * message. 
            * ---
            * Not so fast though, It behooves you to invoke the the endpoint
            * that is supposed / has to be configured for EACH last text menu node.
            */

            $service_data = $this->package_data(
                        $current_menu['menu'][$this->session_data['prev_text_menu_node'] - 1]['ussd_menu_id'],
                        $current_menu['menu'][$this->session_data['prev_text_menu_node'] - 1]['end_point']
                            );
            $this->logger->logInfo('Packaged data => ' .
                            print_r($service_data, 1)
                        );
            $set = [ 'state' => 'COMPLETE' ];
            $where = [ 
                       'ussd_requests_id' => $this->db_request_id
                     ];
            if (
                $this->db->update('ussd_request', $set, $where)
               ) {
                $this->logger->logInfo('Successfully marked session as ' .
                                       'complete in the database'
                                      );
            }
            else {
                $this->logger->logInfo('Update failed.');
            }
                

            /* Start_Here */
            $ep = new Endpoint($service_data);
            /* After acheiving the haul that you have thus far, you can safely
             * proceed to updating the most recent ussd_request record which
             * is the current one to COMPLETE
             */

        }

        /* Prepend the menu prefix CON / END */
        $this->set_menu_prefix( $this->is_last_menu );
        $menu_text = $header . PHP_EOL . $menu_text;
        $menu_text = $this->prepend_prefix($menu_text);
        $this->logger->logInfo("This is the menu text => " . $menu_text);
        return $menu_text;

    }
    
    private function set_menu_prefix ( $key ) {
        $prefixes = [
                 'CON', 'END',
        ];
        $this->menu_prefix = $prefixes[$key];
    }

    private function prepend_prefix ( $menu_text ) {
        $fmt = '%s %s';
        return sprintf($fmt, $this->menu_prefix, $menu_text);
    }
        

    /* Prints the string to be displayed on the user's phone */
    public function print_ussd_string () {
    }

    private function get_shortcut_string () {
        $reg = '/' . $this->service_code . '\*/';
        return preg_replace($reg, '', $this->shortcut);
    }
    /* Menus are cached with the key defined as follows:
     * service_code[*shortcut]
     * Note: The section in square brackets is optional.
     */
    public function set_menu () {
        if ($this->shortcut) {
            $key = $this->service_code . '*' . $this->get_shortcut_string();
        }
        else {
            $key = $this->service_code;
        }

        $cache_data = $this->cache->retrieve_from_cache($key);

        /* The Cache'd menu */
        $menu = json_decode($cache_data['results'], true);

        /* If the menu was not found in cache fetch it from the DB
         * and cache it.
         */
        
        if (!$menu) {
            if ($this->shortcut)
                $query_shortcut = '*' .  $this->shortcut . '#';
            else 
                $query_shortcut = false;
            $menu = $this->db->get_menu($this->service_code, $query_shortcut);

            if ( $menu ) {
                /* Cache menu for a day */
                $this->cache->save_to_cache($key, json_encode($menu), 60*60*24);
                $this->menu = $menu;
            }
            else
                /* The menu does not exist. Exiting */
                $this->menu = null;

        }
        else {
            $this->menu = $menu;
        }
    }

   //NOTE: Add prev_text_menu_node to cache <= Done
   //Prev node is the index of the previous menu displayed <= ??
    private function get_text_menu_text($menu) {
        $prev_text_menu_node = isset($this->session_data['prev_text_menu_node']) ?
                    /* True */
                    $this->session_data['prev_text_menu_node'] :
                    /* False */
                    false;  
        /* Check if there was a previous node */
        if ( $prev_text_menu_node ) {
            /* Check if we're on the last text node */
            if ( $prev_text_menu_node == count( $menu ) ) {
                $this->is_last_menu = 1;
                return false;
            }
            /* There are still some nodes to go. */
            $text_menu = $menu[$prev_text_menu_node]['menu_name'];
            $cache_value = ++$prev_text_menu_node;
        }
        /* Return this node */
        else {
            $cache_value = 1;
            $text_menu = $menu[$cache_value - 1]['menu_name'];    
        }
        $this->add_to_cache_data('prev_text_menu_node', $cache_value);
        return $text_menu;
    }
    
    private function persist_cache_data () {
        $key = $this->service_code . '#' . $this->msisdn;
        $data = $this->cache_data;
        $this->cache->save_to_cache($key, json_encode($data), 200);
    }

    /* Appends the supplied details to the data to be cache'd */
    private function add_to_cache_data ( $key, $value ) {
        $this->cache_data = array_merge( 
                    $this->cache_data,
                    [ $key => $value ]
                );
    }

   public function get_menu_node($menu, $node, $start=false){
        $node_arr = split('\.', $node);
        $new_node = substr($node, strpos($node, '.'), strlen($node));
        $menu = $start? $menu[$node_arr[0]]:$menu['menu'][$node_arr[0]];
        
        if(strlen($new_node) > 0){
            if(empty($menu) || count($menu) == 0){
                return false;
            }
            $node_index = isset($node_arr[0])?$node_arr[0]:0;
            return get_menu_node($menu[$node_index], $new_node);
        }
        return $menu;
    }

    private function get_current_menu ($node){
        $menu_copy = $this->menu;
        $node_arr = split('\.', $node);
        $traverse = function ($val, $key) use (&$menu_copy) {
            if ($key == 0)
                $menu_copy = $menu_copy[$val];
            else
                $menu_copy = $menu_copy['menu'][$val];
        };

        /* Traverse menu */
        array_walk($node_arr, $traverse);
        /*
         *  $this->logger->logInfo("Current Menu: " . print_r($menu_copy, 1));
         */
        return $menu_copy;
    }

    /* Returns an array with the menu at the current level */
    private function get_current_menu_legacy ($node){
        $node_arr = split('\.', $node);
        $traverse = function ($ca, $v) use ($node_arr) {
            if (count($node_arr) % 2) 
                return $ca[$v];
            else
                return $ca['menu'][$v];
        };

        $this->set_menu();
        /* Traverse menu */
        $last_menu =  array_reduce($node_arr, $traverse, $this->menu);
        return $last_menu;
    }

    /* For navigation nodes check the ussd string decrementing
     * by one the values in the ussd string to construct a 
     * node string eg
     * if ussd_string is 2*3*4*1 the node string becomes
     * 0.1.2.3.0
     * Notice that a 0 is prepended for the initial dial.
     */
    private function get_current_node_from_ussd_string () {
        $ussd_str = $this->clean_ussd_string();
        $us = split('\*', $ussd_str);
        array_walk($us, function ($v, $k) use (&$us) {
            $us[$k] = $v - 1;
        });

        return '0.' . join('.', $us);
    }

        
    private function node_to_array ( $node ) {
        return split('\.', $node);
    }
    /* Retuns a string representation of the
     * traversal instructions to be applied
     * to obtain the current menu.
     */
    private function get_current_node() {
        $last_node = isset($this->session_data['last_node']) ?
            $this->session_data['last_node'] : false;
        $last_node_type =  isset($this->session_data['last_node_type']) ?
            $this->session_data['last_node_type'] : false;

        $this->logger->logInfo("Cache'd Node Info => \nNode: " . $last_node .
                "\nNode Type: " . $last_node_type);
        /* This is NOT a fresh session and the
         * node selected is a navigation
         * node. ie the selection is NOT /
         * DOES NOT have an endpoint
         * Input is decremented since php arrays
         * are zero indexed.
         */
        if (
            /* $last_node is set */
            ($last_node != NULL)
            &&
            ($last_node_type != 'TEXT')
           ) {
            $current_node = $this->get_current_node_from_ussd_string();
        }

        /* Session is NOT fresh and
         * selected node is a TEXT node
         */
         else if (
                   /* $last_node is set */
                   ($last_node != NULL)
                   &&
                   /* $last_node_type is TEXT */
                   ($last_node_type == 'TEXT')
                 ) {
              $current_node = $last_node;
         }

        /* Session is fresh */
        else {
            $current_node = '0';
        }
        $this->add_to_cache_data('last_node', $current_node);
        $this->logger->logInfo("Current Node => " . $current_node);
        return $current_node;
        
    }
    
    /* Returns text comprising of the names of the
     * menu item names.
     */
    public function get_menu_children ( $menu ) {
        $index=0;
        $get_menu_text = function($cur, $value) use (&$index) {
            return $cur . ++$index . '. ' . $value['menu_name'] . PHP_EOL;
        };

        $menu_text = array_reduce($menu, $get_menu_text);
            
        return $menu_text;
    }

    public function get_next_child_menu ( $menu, $node ) {
        $menu_text = ++$node . '. ' . $menu[$node]['menu_name'] . PHP_EOL;
        return $menu_text;
    }

    private function parse_response ( $data ) {
        $data = json_decode($data);
        $fmt = '%s %s';
        return sprintf($fmt, $data['prefix'], $data['menu_text']);
    }

    private function package_data ( $menu_id, $ep ) {
        return [
                /* Obtained from the current menu */
                'menu_id' => $menu_id,
                'endpoint' => $ep,
                /* Obtained from the current session */
                'shortcut' => $this->shortcut,
                'ussd_request_id' => $this->db_request_id,
                'ussd_string' => $this->ussd_string,
                'operator' => $this->operator,
                'msisdn' => $this->msisdn,
               ];
    }

    public function call_endpoint ( $endpoint, $data = false, $headers = false ) {
        /* Check for and handle headers */
        if ( $headers ) {
            $request = new Url($endpoint, true);
            $request->setHeaders($headers);
        }
        else {
            $request = new Url($endpoint);
        }

        /* Check for and handle data */
        if ( $data ) {
            $request->setPost($data);
        }

        $request->callUrl();
        return $this->parse_response( $request );
    }

    public function __tostring(){
       if ($this->ussd_string_text === false)
           return $this->default_session_end_message;
       else
           return $this->ussd_string_text;
    }
}


/* Routines
 * · __construct
 * · set_ussd_params
 * · get_session_data
 * · clean_ussd_string
 * · ussd_string_level
 * · get_level
 * · get_input
 * · get_shortcut
 * · get_operator
 * · get_regex
 * · match_content_type
 * · content_type
 * · read_data
 * · validate_ussd_params
 * · valid_params
 * · initialize_ussd_settings
 * · valid_methods
 * · is_valid_method
 * · receive_data
 * · get_ussd_string_from_menu
 * · set_menu_prefix
 * · prepend_prefix
 * · print_ussd_string
 * · get_shortcut_string
 * · set_menu
 * · get_text_menu_text
 * · persist_cache_data
 * · add_to_cache_data
 * · get_menu_node
 * · get_current_menu
 * · get_current_menu_legacy
 * · get_current_node_from_ussd_string
 * · node_to_array
 * · get_current_node
 * · get_menu_children
 * · call_endpoint
 */
