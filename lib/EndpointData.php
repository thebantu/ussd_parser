<?php
if (!defined('ROOT')) define('ROOT', '/var/www/html/ussd_request_parser');
require_once(ROOT . '/.autoload/autoload.php');

use Ussd\Logger;
use Utils\Configuration\Config;
/* Endpoint Data */
class EndpointData {
    
    /* Input data */
    protected $data;

    /* Miscallaneous Params */
    protected $menu_id;
    protected $logger;
    protected $config;

    public function __construct ( $data ) {
        $this->data = $data;
        $this->config = Config::get_config();
        $this->logger = new Logger( $this->config['logging']['logfile'] );
        $this->prepare_data();
    }

    private function remove_keys ( $data, $keys ) {
        $remove = function ($v) use (&$data) {
                        unset($data[$v]);
        };
        array_walk($keys, $remove);
        return $data;
    }

    private function get_data () {
        return $this->data;
    }

    private function prepare_data () {
        $this->menu_id = $this->data['menu_id'];
        /* Remove keys obtained from the current menu node */
        $this->remove_keys($this->data, ['endpoint', 'menu_id']);
    }

    public function __get ( $name ) {
        $method = "get_{$name}";
        if ( method_exists( $this, $method )) {
            return $this->$method();
        }
        else {
            return false;
        }
    }

}
