<?php

define('ROOT_DIR', '/var/www/html/ussd_request_parser/lib');

$classes_dir = array (
    /* Lib Dir */
    ROOT_DIR . '/',
    /* Endpoint Classes */
    ROOT_DIR . '/endpoints/',
);

function __autoload($class_name) {
    global $classes_dir;
    foreach ($classes_dir as $directory) {
        if (file_exists($directory . $class_name . '.php')) {
            require_once ($directory . $class_name . '.php');
            return;
        }
    }
}
