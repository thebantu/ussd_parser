<?php

if (!defined('ROOT')) define('ROOT', '/var/www/html/ussd_request_parser');
require_once(ROOT . '/.autoload/autoload.php');

/* Content Endpoint Data */
class ContentData extends EndpointData {
    
    /* Param Keys that are needed to talk to subscribe manage. */
    protected $param_keys = [ 'content_id', 'msisdn', 'shortcode',
        'service_id', 'sdp_id', 'product_id', 'type',
        'sp_id', 'trx_id', 'network'
    ];

    protected $db_configs;

    public $operator;
    /* Miscallaneous Params */
    protected $misc_params;

    public function __construct ( $data ) {
        parent::__construct( $data );
        $this->logger->LogDebug("Initialized with this data => " .
                                   print_r($this->data, 1)
                               );
        $this->get_db_data();
        $this->process_misc_data();
    }

    private function add_misc_param ( $arr, $key, $val ) {
        return array_merge( $arr, [ $key => $val ] );
    }

    private function process_misc_data () {
        $param_bag = [];
        $this->operator = $this->data['operator'];
        array_walk( $this->param_keys, function ( $v, $k ) 
                                       use (&$param_bag) {
            $this->add_misc_param ( 
                    /* Array to save data to */
                    $param_bag,
                    /* Param Key */
                    $v,
                    /* Param Value */
                    $this->menu_id
            );
        });
        $this->set_misc_params( $param_bag );
        $this->append_misc_params();
    }

    private function set_misc_params ( $params ) {
        if ( is_array( $params ) && $params ) {
            foreach ( $params as $param_key => $v ) {
                $this->set_param( $param_key );
                $this->logger->LogDebug(
                        "Content Data: Key: " .
                        $param_key
                );
            }
        }
        else {
            return false;
        }
    }

    private function get_db_data () {
         $db = new Db;
         $this->db_configs = $db->retreive_service_details( $this->menu_id );
    }
    
    private function set_param ( $param ) {
        /* Return callback that corresponds to the
         * param name set
         */

         /* Keyword param */
         $keyword = function () use ( $param ) {
             $this->misc_params[$param] = 'contents';
         };

         /* Type of request 0 - Addition (type) */
         $type = function () use ( $param ) {
             $this->misc_params[$param] =  0;
         };

         /* Transaction Id (trx_id) */
         $trx_id = function () use ( $param ) {
             $this->misc_params[$param] = $this->data['ussd_request_id'];
         };

         /* Mobile number (msisdn)  */
         $msisdn = function () use ( $param ) {
             $this->misc_params[$param] = $this->data['msisdn'];
         };

         /* Sdp Product ID (product_id) */
         $product_id = function () use ( $param ) {
             $this->misc_params[$param] = $this->db_configs['sdp_product_id'];
         };

         /* Sdp Service ID (sdp_id) */
         $sdp_id = function () use ( $param ) {
             $this->misc_params[$param] = $this->db_configs['service_name'];
         };

         /* Sp ID first 6 characters of sdp_id */
         $sp_id = function () use ( $param ) {
             $this->misc_params[$param] = preg_replace( '/^(.{6}).*/', "\1", $param );
         };

         /* Local Service ID (service_id) */
         $service_id = function () use ( $param ) {
             $this->misc_params[$param] = $this->db_configs['service_id'];
         };
         
         /* Shortcode (shortcode) */
         $shortcode = function () use ( $param ) {
             $this->misc_params[$param] = $this->db_configs['short_code'];
         };

         /* Content ID (content_id) */
         $content_id = function () use ( $param ) {
             $this->misc_params[$param] = $this->db_configs['content_id'];
         };

         /* Network Operator */
         $network = function () use ( $param ) {
             $this->misc_params[$param] = $this->operator;
         };

         /* Return the callback */
         return $$param();
    }

    private function append_misc_params () {
        array_push($this->data, $this->misc_params);
    }

}
