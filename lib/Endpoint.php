<?php


if (!defined('ROOT')) define('ROOT', '/var/www/html/ussd_request_parser');
require_once(ROOT . '/.autoload/autoload.php');

use \Url as Url;
use Utils\Configuration\Config;
use Ussd\Logger as Logger;

class Endpoint implements EndpointBehaviour
{

    /* Post Processing Instructions */
    const MPESA = 'MPESA';
    const NONE = 'NONE';
    
    /* Endpoint */
    protected $endpoint;
    
    /* Endpoint headers */
    protected $headers = false;

    /* Input data */
    protected $data;

    /* Return values produced by this class */
    protected $endpoint_result;

    /* Configs */
    protected $config;

    /* Post Process defaults to MPESA */
    protected $post_process = NONE;

    /* Is last menu flag */
    protected $is_last_menu = 0;

    /* Logger */
    protected $logger;

    public function __construct ( $data, $misc = null ) {
        /* Get Configs */
        $this->config = Config::get_config();

        $this->logger = new Logger( $this->config['logging']['logfile'] );

        /* Set data destination */
        $this->endpoint = $data['endpoint'];

        /* Handle Misc Data */
        if ( !is_null( $misc ) and is_array( $misc ) ) {
            foreach ( $misc as $item => $value ) {
                /* Create Properties */
                $this->$item = $value;
            }

        }

        $this->handle_request( $data );

    }

    public function handle_request ( $data ) {
        /* Bootstrap the rest of the class with relevant data */
        $this->set_endpoint_data( $data );

        /* Call the endpoint */
        $this->call_endpoint();
    }

    private function set_endpoint_data ( $data ) {
        $this->data = $this->get_endpoint_data ( $data );
    }
    
    private function get_classes_dir() {
        return split(',', $this->config['settings']['classes_dir']);
    }

    private function my_class_exists ( $class ) {
        $classes_dir = $this->get_classes_dir();

        foreach ($classes_dir as $directory) {
            $file = $this->config['settings']['app_root'] .
                        'lib/' .
                        $directory .
                        '/' .
                        $class .
                        '.php';
            if (file_exists( $file )) {
                return true;
            }
        }
        return false;
    }


    private function get_class_name ( $ep ) {
        $path = parse_url( $this->endpoint, PHP_URL_PATH );
        $port = parse_url( $this->endpoint, PHP_URL_PORT );

        if ( $port = 2600 ) {
            if ( $path ) {
                $class_name = $this->gen_class_name( $path );
                if ( $this->my_class_exists( $class_name ) ) {
                    $logger->LogDebug('Using the ' . $class_name . ' class.');
                    return $class_name;
                }
                else {
                    $this->logger->LogDebug('Using the EndpointData class.');
                    return 'EndpointData';
                }
            }
            else {
                return 'EndpointData';
            }
        }
        return 'EndpointData';
    }

    private function gen_class_name ( $path ) {
        $fmt = '%s%s%s';
        $path = strtolower(substr($path, 1, strpos($path, '.')-1));
        return sprintf(
                    $fmt,
                    strtoupper(substr($path, 0, 1)),
                    substr($path, 1),
                    'Data'
                );
    }

    public function __get ( $name ) {
        $method = "get_{$name}";
        if ( method_exists( $this, $method )) {
            return $this->$method();
        }
        else {
            return false;
        }
    }

    public function __set ( $property, $value ) {
        $method = "set_{$property}";
        if (method_exists( $this, $method )) {
            return $this->$method();
        }
        else {
            return false;
        }
    }

    private function get_endpoint_data ( $d ) {
        $class_name = $this->get_class_name($this->endpoint);
        $data = new $class_name( $d );
        return $data->data;
    }

    public function call_endpoint ( ) {
        /* Check for and handle headers */
        $headers = $this->headers;
        if ( $headers ) {
            $request = new Url($this->endpoint, true);
            $request->setHeaders($headers);
        }
        else {
            $request = new Url($this->endpoint);
        }

        /* Check for and handle data */
        if ( $this->data ) {
            $request->setPost($this->data);
        }

        $request->callUrl();
        $this->endpoint_result = $request;
        $this->is_last_menu = 1;
    }

    /* Give the parser instructions to show the mpesa checkout to the user */
    private function get_post_process_instruction () {
        return $this->post_process;
    }

    /* Return the is last menu flag */
    private function get_is_last_menu () {
        return $this->is_last_menu;
    }

    private function get_endpoint_result () {
        return $this->endpoint_result;
    }

    public function get_response_json ( $data ) {
        return json_encode( $data );
    }

    private function pack_ussd_menu ( $data ) {
        return [
                  'menu_text' => $this->endpoint_result
               ];
    }

    public function get_ussd_menu () {
        /*
         * Obsolete since we only need to return the menu text
         *   return $this->get_response_json( 
         *                           $this->pack_ussd_menu(
         *                                   $this->menu_text
         *                           )
         *                 );
         */
         return $this->endpoint_result;
    }

}
