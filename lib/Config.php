<?php
namespace Utils\Configuration;

define("CONF_FILE",'/var/www/html/ussd_request_parser/conf/config.ini');

Class Config {
    static $config = array ();
    public function __construct() {
    }
    public static function get_config() { //Used a static method to avoid the tedium of initializing the Config Class
        self::$config = parse_ini_file(CONF_FILE, true);
        return self::$config;
    }
}

