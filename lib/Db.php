<?php 

namespace Ussd;
/**
 * Database wrapper for a MySQL with PHP tutorial
 * 
 */
require_once( '/var/www/html/ussd_request_parser/lib/Logger.php' );
use Utils\Configuration\Config;
use \mysqli as mysqli;
use \PDO as PDO;
use \PDOException as PDOException;

class Db {
    // The database connection
    protected static $connection;
    protected $pconn;
    protected $log;
    protected $config;

    public function __construct() {
        $this->config = Config::get_config(); 
        $this->log = new Logger($this->config['logging']['logfile']);
        $this->pdo_connect();
    }
    /**
     * PDO's Connect method
     */
    private function get_pdo_opts () {
        return [
                'PDO::ATTR_PERSISTENT' => true,
               ];
    }
    public function pdo_connect() {

        // Try and connect to the database

        // Load configuration as an array. Use the actual location of your configuration file
        // Put the configuration file outside of the document root
        $this->pconn = new
        PDO( 'mysql:host=' . $this->config['database']['host'] .
             ';dbname=' . $this->config['database']['dbname'],
                $this->config['database']['username'],
                $this->config['database']['password'],
                $this->get_pdo_opts()
          );
        $this->pconn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        // If connection was not successful, handle the error
        if($this->pconn === false) {
            // Handle error - notify administrator, log to a file, show an error screen, etc.
            $this->log->LogError("Unable to get connection to the database ...");
        }

    }
    /**
     * Connect to the database
     * 
     * @return bool false on failure / mysqli MySQLi object instance on success
     */
    public function connect() {

        // Try and connect to the database
        if(!isset(self::$connection)) {
            // Load configuration as an array. Use the actual location of your configuration file
            // Put the configuration file outside of the document root
            self::$connection = new
            mysqli( $this->config['database']['host'],
                    $this->config['database']['username'],
                    $this->config['database']['password'],
                    $this->config['database']['dbname']
                  );
        }

        // If connection was not successful, handle the error
        if(self::$connection === false) {
            // Handle error - notify administrator, log to a file, show an error screen, etc.
            $this->log->LogError("Unable to get connection, returing false ...");
            return false;
        }

        return self::$connection;
    }

    /**
     * Query the database
     *
     * @param $query The query string
     * @return mixed The result of the mysqli::query() function
     */
    public function query($query) {
        $this->log->LogDebug("EXCEUTING QUERY: ". $query);
        // Connect to the database
        $connection = $this -> connect();

        // Query the database
        if(strpos($query, ";")>0){
            $result = $connection -> multi_query($query);
        }else{
            $result = $connection -> query($query);
        }

        return $result;
    }
    /**
     * Fetch menu from db, menu are fetched based on parent child link is
     * ussd_menu_table 
     */

    public function get_menu ($service_code, $shortcut) {
        $query = "select ussd_menu_id, concat(menu_name,
        if((additional_text is null), '', concat(\"\n\", additional_text))) menu_name, service_id, menu_category,
        ussd_code,ussd_shortcut, end_point, parent from ussd_menu_table where
        ussd_code = ".$this->quote($service_code). ' ';
	
        $append = $shortcut ? ' and ' . 
                              'ussd_shortcut=' .  $this->quote($shortcut) .
                              ' limit 1' : ' limit 1';

        $query = $query . $append;
        $result = $this->select($query);
        if ($result ==FALSE) {
            return $result;	
        }
        $result[0]['menu'] = $this->get_menu_children($result[0]['ussd_menu_id']);
        return $result;
       
    }
    
    public function get_menu_children($menu_id){
        $query = "select ussd_menu_id, concat(menu_name,
        if((additional_text is null), '', concat(\"\n\", additional_text)))
        menu_name , service_id, menu_category,
	        ussd_code,ussd_shortcut, end_point, parent from
		ussd_menu_table where parent = ".$this->quote($menu_id);
	
        $result = $this->select($query);
        foreach($result as $key =>$row){
            $result[$key]['menu']= $this->get_menu_children($row['ussd_menu_id']);
        }
        return $result;
	
		
    }

    /* Retreive service details to which the menu belongs */
    public function retreive_base_service_details ( $service_id ) {
        $query = " select service_name, ussd_shortcut from service s inner " .	
        " join ussd_menu_table umt on s.service_id = umt.service_id where " .
        " s.service_id = " . $service_id . " limit 1" ;
        $result = $this->select($query);
        if ( $result ) {
            foreach($result as $key => $row){
                return $row;
            }
        }
        else {
            return false;
        }
    }

    /* Retreive service details from the content id */
    public function retreive_service_details ( $data ) {
        $content_id = $this->retreive_content_id( $data );
        $query = " select content_id, content_service.service_id, sdp_product_id, " .
        "sdp_service.service_name, pipe_id, short_code from content_service " .
        "inner join service using (service_id) inner join sdp_product on " .
        "(sdp_product.local_service_id = content_service.service_id) " .
        "inner join sdp_service using (sdp_service_id) inner join shortcode " .
        "on (content_service.service_id = shortcode.service_id and " .
        "shortcode.network_id = 1) " .
        "where content_service.content_id = " . $content_id . " limit 1";
	
        $result = $this->select($query);
        if ( $result ) {
            foreach($result as $key => $row){
                return $row;
            }
        }
        else {
            return false;
        }
    }

    /* Retreive content_name and artist_name using the content_id */
    public function retreive_content_details ( $menu_id ) {
        $query = "select content_name, artist_name from content inner join " .
                     "artist using (artist_id) where content_id = " .
                     $this->retreive_content_id( $menu_id );
        $result = $this->select($query);
        if ( $result ) {
            foreach($result as $key => $row){
                return [
                        'content_name' => $row['content_name'],
                        'artist_name' => $row['artist_name'],
                       ];
            }
        }
        else {
            return false;
        }
    }

    /* Retreive the content id from the ussd_menu_id */
    public function retreive_service_id ( $menu_id ) {
        $query = "select id from service_ussd where menu_id = " .
                    $menu_id . " limit 1";
	
        $result = $this->select($query);
        if ( $result ) {
            foreach($result as $key => $row) {
                return $row['content_id'];
            }
        }
        else {
            return false;
        }
    }

    /* Retreive the content id from the ussd_menu_id */
    public function retreive_content_id ( $menu_id ) {
        $query = "select content_id from content_ussd where ussd_menu_id = " .
                    $menu_id . " limit 1";
	
        $result = $this->select($query);
        if ( $result ) {
            foreach($result as $key => $row){
                return $row['content_id'];
            }
        }
        else {
            return false;
        }
    }

    /* Chack if the menu specified is a service menu */
    public function check_service_menu ( $menu_id ) {
        return ( $this->retreive_service_id( $menu_id ) ? true : false );
    }

    /* Chack if the menu specified is a content menu */
    public function check_content_menu ( $menu_id ) {
        return ( $this->retreive_content_id( $menu_id ) ? true : false );
    }
    /**
     * Fetch rows from the database (SELECT query)
     *
     * @param $query The query string
     * @return bool False on failure / array Database rows on success
     */
    public function select($query) {
        $rows = array();
        $result = $this->query($query);
        if($result === false) {
            $this->log->LogDebug("SELECT RESULT FALSE: ". $this->error());
            return false;
        }
        while ($row = $result->fetch_assoc()) {
            $rows[] = $row;
        }
        return $rows;
    }

    public function log_ussd_request ( $data ) {
         $table = "ussd_request";
         $fields = array_keys( $data );
         $params = array();
         $field_str = join(", ", $fields);

         /* Callback to create :param_name styled params */
         $to_param_names = function ($field) {
             return preg_replace('/^/', ":", $field);
         };

         /* Callback to create the params array */
         $add_param = function ($v, $k) use ($to_param_names, &$params,
                                             $data) {
               $params[$to_param_names($v)] = $data[$v];
         };

         /* Actually Create the params array */
         array_walk($fields, $add_param);

         $param_str = join(", ", array_map($to_param_names, $fields));

         $query = 'insert into ' . $table . ' (' . $field_str .
                  ' ) values (' . $param_str . ')';
         $log_ussd_request = $this->pconn->prepare($query);
         foreach ($params as $p => $v) $log_ussd_request->bindValue($p, $v);
         try {
             $log_ussd_request->execute();
             $id = $this->pconn->lastInsertId();
             $this->log->LogDebug("Logged ussd_requests with this ID => " . $id);
             return $id;
         }
         catch (PDOException $e) {
             if ($log_ussd_request->errorInfo()[1] == '1062') {
                $this->log->LogError("Got a duplicate record error -> " .
                     $log_ussd_request->errorInfo()[2]
                 );
                 return false;
             }
             else {
                $this->log->LogError("Got this plain error -> " .
                        $log_ussd_request->errorInfo()[2]
                );
                return false;
             }
         }
    }

    public function update ( $table, $set, $where ) {
        /* Callback to create :param_name styled params */
        $to_param_names = function ($field) {
            return preg_replace('/^/', ":", $field);
        };
        $params = [];
        $data = array_merge($set, $where);

        /* Callback to create the params array */
        $add_param = function ($v, $k) use ($to_param_names, &$params,
                                            $data
                                            )
        {
              $params[$to_param_names($k)] = $data[$k];
        };

        /* Actually Create the params array */
        array_walk($data, $add_param);

        /* Construct set String */
        $process_set = function ($carry, $val) use ($set) {
            $key = array_search($val, $set);
            return $carry . $key . " = :" . $key . ", ";
        };
        $regex = '/,\s*$/';
        $set = preg_replace( $regex, "",
            array_reduce($set, $process_set)
        );

        /* Construct where String */
        $process_where = function ($carry, $val) use ($where) {
            $key = array_search($val, $where);
            return $carry . $key . " = :" . $key . " and ";
        };
        $regex = '/\s*(?:AND|OR)\s*$/i';
        $where = preg_replace( $regex, "",
            array_reduce($where, $process_where)
        );
        $query = "update " . $table . " set " . $set . " where " . $where;
        $update = $this->pconn->prepare( $query );
        foreach ($params as $p => $v) $update->bindValue($p, $v);
        $this->log->LogDebug("Params => " . print_r($params, 1));
        try {
            $update->execute();
            $this->log->LogDebug("Update performed successfully.");
            return true;
        }
        catch (PDOException $e) {
           $this->log->LogError($e->getMessage());
           return false;
        }
    }

    /**
     * Fetch the last error from the database
     * 
     * @return string Database error message
     */
    public function error() {
        $connection = $this -> connect();
        return $connection -> error;
    }

    /**
     * Quote and escape value for use in a database query
     *
     * @param string $value The value to be quoted and escaped
     * @return string The quoted and escaped string
     */
    public function quote($value) {
        $connection = $this -> connect();
        return "'" . $connection -> real_escape_string($value) . "'";
    }
}
