<?php

interface EndpointBehaviour
{
    public function handle_request($data);
}
