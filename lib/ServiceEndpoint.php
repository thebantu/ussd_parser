<?php


if (!defined('ROOT')) define('ROOT', '/var/www/html/ussd_request_parser');
require_once(ROOT . '/.autoload/autoload.php');

use \Url as Url;
use Utils\Configuration\Config;

class ServiceEndpoint extends Endpoint implements EndpointBehaviour
{
    
    const MPESA_PROMPT = "Enter your service pin in the next menu to pay" .
                         " for the title deed confirmation fee.\n" .
                         "Please note that this amount will be deducted" .
                         " from your MPESA balance:\n1. Accept\n2. Decline";
    protected $db;

    public function __construct ( $data, $misc = null ) {
        $db = new Db;
        parent::__construct(
    }
    
    /* Perform work using the data */
    public function handle_request ( $data ) {
        /* Service Payment Check */
        if ($data['requires_payment'] > 0) {
            $this->logger->LogDebug('This request requires payment.');
            $this->process_service_menu($data['requires_payment']);
        }
        /* */
        else {
            $this->logger->LogDebug('This request is gonna go through and' .
                                    'through'
                                   );
            /* Bootstrap the rest of the class with relevant data */
            $this->set_endpoint_data( $data );

            /* Call the endpoint */
            $this->call_endpoint();
        }
    }

    /* Service Menus need this */
    private function process_service_menu ( $flag ) {
        /* Take care of content menus yet to be presented with the payment
         * options. The assumption is content menu's imply that they are 
         * text nodes ie not navigation nodes.
         */

        $menu_text = '';

        if ( $flag == 1 ) {
            $this->logger->LogDebug('Returning ' . MPESA_PROMPT . ' as the ' .
                                    'menu text.'
                                   );
            $menu_text = MPESA_PROMPT;
        }

        /* Take care of content menus whose selected payment option is
         * airtime.
         */
        else if ( $flag == 2 ) {
            if ( $this->input == 1 ) {
                $this->logger->LogDebug('Returning an empty string as the' .
                                        'menu';
                                       );
                $this->logger->LogDebug('Setting post_process to ' . MPESA);
                $this->post_process = MPESA;
            }
            $this->is_last_menu = 1;
        }

        $this->endpoint_result = $menu_text;
    }

}
