<?php 

namespace Ussd;
/**
 * Database wrapper for a MySQL with PHP tutorial
 * 
 */
use Utils\Configuration\Config;
use \mysqli as mysqli;

class Db {
    // The database connection
    protected static $connection;
    protected $log;
    protected $config;

    public function __construct() {
        $this->config = Config::get_config(); 
        $this->log = new Logger($this->config['logging']['logfile']);
    }
    /**
     * Connect to the database
     * 
     * @return bool false on failure / mysqli MySQLi object instance on success
     */
    public function connect() {

        // Try and connect to the database
        if(!isset(self::$connection)) {
            // Load configuration as an array. Use the actual location of your configuration file
            // Put the configuration file outside of the document root
            self::$connection = new
            mysqli( $this->config['database']['host'],
                    $this->config['database']['username'],
                    $this->config['database']['password'],
                    $this->config['database']['dbname']
                  );
        }

        // If connection was not successful, handle the error
        if(self::$connection === false) {
            // Handle error - notify administrator, log to a file, show an error screen, etc.
            $this->log->LogError("Unable to get connection, returing false ...");
            return false;
        }
        return self::$connection;
    }

    /**
     * Query the database
     *
     * @param $query The query string
     * @return mixed The result of the mysqli::query() function
     */
    public function query($query) {
        $this->log->LogDebug("EXCEUTING QUERY: ". $query);
        // Connect to the database
        $connection = $this -> connect();

        // Query the database
        if(strpos($query, ";")>0){
            $result = $connection -> multi_query($query);
        }else{
            $result = $connection -> query($query);
        }

        return $result;
    }
    /**
     * Fetch menu from db, menu are fetched based on parent child link is
     * ussd_menu_table 
     */

    public function get_menu ($service_code, $shortcut) {
        $query = "select ussd_menu_id, menu_name, service_id, menu_category,
        ussd_code,ussd_shortcut, end_point, parent from ussd_menu_table where
        ussd_code = ".$this->quote($service_code). ' ';
	
        $append = $shortcut ? ' and ' . 
                              'ussd_shortcut=' .  $this->quote($shortcut) .
                              ' limit 1' : ' limit 1';

        $query = $query . $append;
        $result = $this->select($query);
        if ($result ==FALSE) {
            return $result;	
        }
        $result[0]['menu'] = $this->get_menu_children($result[0]['ussd_menu_id']);
        return $result;
       
    }
    
    public function get_menu_children($menu_id){
        $query = "select ussd_menu_id, item_name, service_id, menu_category,
	        ussd_code,ussd_shortcut, end_point, parent from
		ussd_menu_table where parent = ".$this->quote($menu_id);
	
        $result = $this->select($query);
        foreach($result as $key =>$row){
            $result[$key]['menu']= $this->get_menu_children($row['ussd_menu_id']);
        }
        return $result;
		
    }

    /* Retreive service details to which the menu belongs */
    public function retreive_base_service_details ( $service_id ) {
        $query = " select service_name, ussd_shortcut from service s inner " .
        "join ussd_menu_table umt on s.service_id = umt.service_id where " .
        "s.service_id = " . $service_id . " limit 1";
        	
        $result = $this->select($query);
        if ( $result ) {
            foreach($result as $key => $row){
                return $row;
            }
        }
        else {
            return false;
        }
    }

    /* Retreive service details from the content id */
    public function retreive_service_details ( $content_id ) {
        $query = " select content_id, content_service.service_id, sdp_product_id, " .
        "sdp_service.service_name, pipe_id, short_code from content_service " .
        "inner join service using (service_id) inner join sdp_product on " .
        "(sdp_product.local_service_id = content_service.service_id) " .
        "inner join sdp_service using (sdp_service_id) inner join shortcode " .
        "on (content_service.service_id = shortcode.service_id and " .
        "shortcode.network_id = 1) " .
        "where content_service.content_id = " . $content_id . " limit 1";
	
        $result = $this->select($query);
        if ( $result ) {
            foreach($result as $key => $row){
                return $row;
            }
        }
        else {
            return false;
        }
    }

    /* Retreive the content id from the ussd_menu_id */
    public function retreive_content_id ( $menu_id ) {
        $query = "select content_id from content_ussd where ussd_menu_id = " .
                    $menu_id . " limit 1";
	
        $result = $this->select($query);
        if ( $result ) {
            foreach($result as $key => $row){
                return $row['content_id'];
            }
        }
        else {
            return false;
        }
    }
    /**
     * Fetch rows from the database (SELECT query)
     *
     * @param $query The query string
     * @return bool False on failure / array Database rows on success
     */
    public function select($query) {
        $rows = array();
        $result = $this -> query($query);
        if($result === false) {
            $this->log->LogDebug("SELECT RESULT FALSE: ". $this->error());
            return false;
        }
        while ($row = $result -> fetch_assoc()) {
            $rows[] = $row;
        }
        return $rows;
    }

    /**
     * Fetch the last error from the database
     * 
     * @return string Database error message
     */
    public function error() {
        $connection = $this -> connect();
        return $connection -> error;
    }

    /**
     * Quote and escape value for use in a database query
     *
     * @param string $value The value to be quoted and escaped
     * @return string The quoted and escaped string
     */
    public function quote($value) {
        $connection = $this -> connect();
        return "'" . $connection -> real_escape_string($value) . "'";
    }
}
