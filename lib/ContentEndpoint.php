<?php


if (!defined('ROOT')) define('ROOT', '/var/www/html/ussd_request_parser');
require_once(ROOT . '/.autoload/autoload.php');

use \Url as Url;
use Utils\Configuration\Config;

class ContentEndpoint extends Endpoint implements EndpointBehaviour
{
    
    const PAYMENT_PROMPT = "Buy this song via:\n1. Airtime\n2. MPESA";
    const AIRTIME_PROMPT = "Select YES on next menu to get %s by " .
                           "%s to your phone";
    const MPESA_PROMPT = "Enter your bonga pin in the next menu to get" .
                         " %s by %s to your phone. Don't have Bonga " .
                         "Pin? Dial *126*5#";

    protected $db;

    public function __construct ( $data, $misc = null ) {
        $db = new Db;
        parent::__construct(
    }
    
    /* Perform work using the data */
    private function handle_request ( $data ) {
        /* Content Check */
        if ($data['is_content'] > 0) {
            $this->process_content_menu( $data['is_content']);
        }
        /* */
        else {
            /* Bootstrap the rest of the class with relevant data */
            $this->set_endpoint_data( $data );

            /* Call the endpoint */
            $this->call_endpoint();
        }
    }

    /* Content Menus need this */
    private function process_content_menu ( $flag ) {
        /* Take care of content menus yet to be presented with the payment
         * options. The assumption is content menu's imply that they are 
         * text nodes ie not navigation nodes.
         */
        
        $menu_text = '';

        if ( $flag == 1 ) {
            $menu_text = PAYMENT_PROMPT;

        }

        /* Take care of content menus whose selected payment option is
         * airtime.
         */
        else if ( $flag == 2 ) {
            $content_details = $this->db->retreive_content_details(
                                            $this->data['menu_id']
                               );
            if ( $this->input == 1 ) {
                $menu_text = AIRTIME_PROMPT;                
                $menu_text = vsprintf( $menu_text, $content_details);
            }
            else if ( $this->input == 2 ) {
                $menu_text = MPESA_PROMPT;
                $menu_text = vsprintf( $menu_text, $content_details);
                $this->post_process = MPESA;
            }

        }
        $this->endpoint_result = $menu_text;
    }

}
