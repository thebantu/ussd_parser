<?php

require_once('../.autoload/autoload.php');

/* Endpoint Data */
class EndpointData {
    
    /* Input data */
    protected $data;

    /* Miscallaneous Params */
    protected $menu_id;

    protected function __construct ( $data ) {
        $this->data = $data;
        $this->prepare_data();
    }

    private function remove_keys ( $data, $keys ) {
        $remove = function ($v) use (&$data) {
                        unset($data[$v]);
        };
        array_walk($keys, $remove);
        return $data;
    }

    private function prepare_data () {
        $this->menu_id = $this->data['menu_id'];
        /* Remove keys obtained from the current menu node */
        $this->remove_keys($this->data, ['endpoint', 'menu_id']);
    }

}
