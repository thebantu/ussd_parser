<?php

class Url {
     protected $_useragent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1';
     protected $_url;
     protected $_followlocation;
     protected $_timeout;
     protected $_maxRedirects;
     protected $_cookieFileLocation = './cookie.txt';
     protected $_post;
     protected $_headers;
     protected $_postFields;
     protected $_referer ="http://www.google.com";

     protected $_capath = null;
     protected $_session;
     protected $_webpage;
     protected $_includeHeader;
     protected $_noBody;
     protected $_error;
     protected $_status;
     protected $_binaryTransfer;
     public    $authentication = 0;
     public    $auth_name      = '';
     public    $auth_pass      = '';

     public function __construct($url,$includeHeader = false,$followlocation = true,$timeOut = 30,$maxRedirecs = 4,$binaryTransfer = false,$noBody = false)
     {
         $this->_url = $url;
         $this->_followlocation = $followlocation;
         $this->_timeout = $timeOut;
         $this->_maxRedirects = $maxRedirecs;
         $this->_noBody = $noBody;
         $this->_includeHeader = $includeHeader;
         $this->_binaryTransfer = $binaryTransfer;

         $this->_cookieFileLocation = dirname(__FILE__).'/cookie.txt';

     }

     public function setHeaders($headers){
       $this->headers = $headers;
     }

     public function useAuth($use){
       $this->authentication = 0;
       if($use == true) $this->authentication = 1;
     }

     public function setName($name){
       $this->auth_name = $name;
     }
     public function setPass($pass){
       $this->auth_pass = $pass;
     }

     public function setReferer($referer){
       $this->_referer = $referer;
     }

     public function setCookiFileLocation($path)
     {
         $this->_cookieFileLocation = $path;
     }

     public function setPost ($postFields)
     {
        $this->_post = true;
        $this->_postFields = $postFields;
     }

     public function setUserAgent($userAgent)
     {
         $this->_useragent = $userAgent;
     }

     public function setCAPath ($path) {
	     $this->_capath = $path;
     }

     public function callUrl($url = 'nul')
     {
        if($url != 'nul'){
          $this->_url = $url;
        }

         $s = curl_init();

         curl_setopt($s,CURLOPT_URL,$this->_url);
         curl_setopt($s,CURLOPT_HTTPHEADER,array('Expect:'));
         curl_setopt($s,CURLOPT_TIMEOUT,$this->_timeout);
         curl_setopt($s,CURLOPT_MAXREDIRS,$this->_maxRedirects);
         curl_setopt($s,CURLOPT_RETURNTRANSFER,true);
         curl_setopt($s,CURLOPT_FOLLOWLOCATION,$this->_followlocation);
         curl_setopt($s,CURLOPT_COOKIEJAR,$this->_cookieFileLocation);
         curl_setopt($s,CURLOPT_COOKIEFILE,$this->_cookieFileLocation);
	 curl_setopt($s, CURLOPT_SSL_VERIFYPEER, false);

         if($this->authentication == 1){
           curl_setopt($s, CURLOPT_USERPWD, $this->auth_name.':'.$this->auth_pass);
         }
         if($this->_post)
         {
             curl_setopt($s,CURLOPT_POST,true);
             curl_setopt($s,CURLOPT_POSTFIELDS,$this->_postFields);

         }

         if($this->_includeHeader)
         {
           curl_setopt($s,CURLOPT_HEADER,true);
	       curl_setopt($s, CURLOPT_HTTPHEADER, $this->headers);
         }
	 
	 if (!is_null($this->_capath))
	 {
		curl_setopt($s, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($s, CURLOPT_CAINFO, $this->_capath);
	 }

         if($this->_noBody)
         {
             curl_setopt($s,CURLOPT_NOBODY,true);
         }
         /*
         if($this->_binary)
         {
             curl_setopt($s,CURLOPT_BINARYTRANSFER,true);
         }
         */
         curl_setopt($s,CURLOPT_USERAGENT,$this->_useragent);
         curl_setopt($s,CURLOPT_REFERER,$this->_referer);

         $this->_webpage = curl_exec($s);
	 if ( $this->_webpage === false )
		 $this->_error = curl_error($s);
	 else
		 $this->_error = "none";

	 $this->_status = curl_getinfo($s,CURLINFO_HTTP_CODE);
         curl_close($s);

     }

   public function getError()
   {
	return $this->_error;
   }
   public function getHttpStatus()
   {
       return $this->_status;
   }
   public function header_list () {
       $header_keys = [
           'ctype:xml', 'ctype:json', 'ctype:post', 'len', 'soap'
           ];
       return $header_keys;
   }

    /* Utility function to get single http headers */
    public function get_header( $key, $misc = '', $url ) {
        if ($url )
            $endpoint = $url;

        /* List of headers in no specific order */
        $headers = [
        'ctype:xml' => 'Content-type: text/xml;charset="ISO-8859-1"',
        'ctype:json' => 'Content-type: application/json',
        'ctype:post' => 'Content-type: application/x-www-form-urlencoded',
        'soap' => 'SOAPAction: ' . $charging_endpoint,
        'len' => 'Content-length: ' . $misc,
        ];
        return $headers[$key];
    }

    /* Utility function to generate an array of header to be sent in a 
     * payload
     */
     public function gen_headers ( $keys ) {
          /* -------------------------------------------------- */ 
          /* | Callbacks |
          /* -------------------------------------------------- */
          /* Remove erroneous keys */
          $remove_key = function ($key) use (&$keys) {
              unset($keys[$key]);
          };

          /* Push headers into the return array */
          $fetch_headers = function ($key) use (&$rdata) {
              array_push($rdata, get_header($key));
          };
          /* -------------------------------------------------- */

          /* -------------------------------------------------- */ 
          /* | Main |
          /* -------------------------------------------------- */ 
          $official_header_list = header_list();
          $rdata = [];

          $problem_keys = array_diff($keys, $official_header_list);
          if ( count($problem_keys) ) {
              array_walk($problem_keys, $remove_key);
          }
          array_walk($keys, $fetch_headers);
          /* -------------------------------------------------- */ 

          return $rdata;
      }

      public function __tostring(){
         if ($this->_webpage === false)
             return $this->_error;
         else
             return $this->_webpage;
      }
}
