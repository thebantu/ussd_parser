<?php

require_once('inc/paths.php');

use Utils\Configuration\Config;

/* The main USSD Request Class */
class Request {
    protected $service_code;
    protected $ussd_string;
    protected $session_id;
    protected $msisdn;
    protected $input;
    protected $operator;
    protected $level;
    protected $shortcut;
    protected $menu;
    protected $logger;
    protected $config;
    protected $valid_methods;
    protected $acceptable_params;
    protected $default_session_end_message;
    protected $ussd_string_text;
    private $session_data = false;
    private $cache_data = [];
    private $cache = null;
    private $db = null;


    public function __construct () {
        $this->initialize_ussd_settings();
    }

    private function set_ussd_params ( $data ) {
        $this->service_code = $data['service_code'];
        $this->msisdn = $data['msisdn'];
        $this->ussd_string = urldecode($data['ussd_string']);
        $this->session_id = $data['session_id'];
        $this->add_to_cache_data('session_id', $this->session_id);
    }

    //NOTE: Cache key example: 699#254712171204
    private function get_session_data () {
        $key = $this->service_code . "#" . $this->msisdn;
        return $this->cache->retrieve_from_cache($key);
    }

    private function get_level () {
        /* This session is NOT fresh */
        if ($this->session_data) {
            $level = $this->session_data['level']++;
        }
        /* This session IS fresh */
        else {
            $level = 0;
        }
        $this->add_to_cache_data('level', $level);
        return $level;
    }

    /* When input is false it is either not there at all
     * it was a shortcut's first dial ie the contents of
     * ussd_string make up the shortcut.
     */ 
    public function get_input () {
        if ( $this->level > 0 )
            return preg_replace(get_regex('last_input'), "$1", $this->ussd_string);
        else
            return false;
    }

    public function get_shortcut () {
        /* This session is NOT fresh */ 
        if ($this->session_data) {
            $shortcut = $this->session_data['shortcut'];
        }
        /* This session is fresh and ussd_string is NOT empty */
        else if ( $this->level == 0 and ! empty($this->ussd_string) ) {
            $shortcut = $this->service_code . $this->ussd_string;
        }
        /* No Shortcut was found */
        else
            $shortcut = false;
        $this->add_to_cache_data('shortcut', $shortcut);
        return $shortcut;
    }

    public function get_operator () {
        preg_match_all(get_regex('op_code'), $this->msisdn, $results);
        $op_code = isset($results[1]) ?: 2;
        switch ($op_code) {
            case 0: case 1: case 2: case 9:
                $operator = "safaricom";
                break;
            case 5: case 3: case 8:
                $operator = "airtel";
                break;
            case 7:
                $operator = "orange";
                break;
            default:
                $operator = "safaricom";
        }
        return $operator;
    }

    public function get_regex( $key ) {
        /* Add new regexes to this $regexes array
         * when calling it the key should correspond to
         * the array's key
         */
        $regexes = [
            /* Valid json content */
            'json' => '!application/json!',
            /* Valid xml content */
            'xml' => '!application/xml!',
            /* Valid plain text content */
            'text' => '!text/plain!',
            /* Valid html content */
            'html' => '!text/html!',
            /* HTTP POST */
            'post' => '!application/x-www-form-urlencoded!',
            /* Valid Kenyan MSISDN.
             * If ever this app is to be used by ussd requests
             * a proper function for identifying a valid country's
             * MSISDN should be implemented.
             */
            'mobile_num' => '/(?:0|254)?(7\d{8})/',
            /* Find digits only. */
            'digits' => '/^\d+$/',
            /* Get special characters inside a field name. */
            'field_special_chars' => '/[.]/',
            /* Positive match test for content type check */
            'content_type_positive_test' => '/7\~/',
            /* Look for error string */
            'error_str' => '/^(ERROR|Notice)/i',
            /* Get Op code */
            'op_code' => '/^(?:254|0)?7(.)/',
            /* Get last input */
            'last_input' => '/.*?\*?([^*]+)$/',

        ];
        return $regexes[$key];
    }

    private function match_content_type ( $content_type ) {
        $valid_content_types = [ 'post', 'json', 'text', 'xml' ];

        /* 7 - Success
         * 6 - Failure
         * On match append the content type to 7
         */
        $check_content_type = function ( $test ) use ( $content_type ) {
            return (preg_match(get_regex( $test ), $content_type) ? "7~" . $test : 6);
        };
    
        /* Check if a valid content type was found */
        $positive_match = function ( $res ) {
            return preg_match(get_regex('content_type_positive_test'), $res);
        };

        $results = array_map( $check_content_type, $valid_content_types );
        $matches = array_values(array_filter( $results, $positive_match ));
        return (count( $matches ) > 0) ? substr( $matches[0],2 ) : false;
    }
        
    /* Get the content type of the request body */
    private function content_type () {
        return match_content_type( $_SERVER['CONTENT_TYPE']);
    }

    /* Read the request body for the POST */
    private function read_data () {
        return file_get_contents('php://input');
    }

    /* Check the parameters of the submitted data 
     * against an acceptable list of params
     */
    private function validate_ussd_params ( $data ) {
        return !count(array_diff(array_keys($data), $this->acceptable_params));
    }

    private function valid_params () {
        return split(',', $this->configs['settings']['acceptable_params']);
    }

    private function initialize_ussd_settings () {
        /* Read App Config */
        $this->config = Config::get_config();
        $this->valid_methods = $this->valid_methods();
        $this->acceptable_params = $this->valid_params();

        /* Bootstrap some resources */
        $this->cache = new MemcacheLib(200);
        $this->db = new Db;
        $this->logger = new Logger($this->config['logging']['logfile']);

        /* Bootstrap USSD */
        $this->set_ussd_params($this->receive_data());
        $this->operator = $this->get_operator();
        $this->session_data = $this->get_session_data(); 
        $this->shortcut = $this->get_shortcut();
        $this->level = $this->get_level();
        $this->input = $this->get_input();
        $this->ussd_string_text = $this->get_ussd_string_from_menu();
    }
    
    private function valid_methods () {
        return split(',', $this->configs['settings']['valid_methods']);
    }

    /* Only POST is valid */
    function is_valid_method () {
        return ( in_array($_SERVER['REQUEST_METHOD'], 
            array_map('strtoupper', valid_methods()))
        );
    }

    /* Parse received data */
    private function receive_data () {
        if (is_valid_method()) {
            $type = content_type();
            $data = read_data();
        }
        else
        return false;
        switch ( $type ) {
          case "json":
              $rdata = json_decode($data);
              break;
          case "xml":
              $rdata = parse_xml($data);
              break;
          case "post":
              parse_str($data, $rdata);
              break;
        }
        if (validate_ussd_params($rdata))
            return $rdata;
        else
            return false;
    }

    /**
     * For Text menu to work -- We create a parent menu with menu_category TEXT
     * Have all the children to this parent as TEXT Menus as well
     */
    public function get_ussd_string_from_menu () {
        /* Get the current node from the tree */
        $current_node = $this->get_current_node();

        /* Fetch the current menu and header. */
        $current_menu =  $this->get_current_menu($current_node);
        $header = $current_menu['item_name'];

        /* Get the current node type from the tree
         * and save it to cache_data.
         */
        $current_node_type = $current_menu['menu_category'];
        $this->add_to_cache_data('last_node_type', $current_node_type);

        /* Get the endpoint if at all it is set */
        if ( $current_menu['endpoint'] )
            $endpoint = $current_menu['endpoint'];
        
        /* Current node is a navigation node */
        if( $current_node_type != 'TEXT' ){
            $menu_text = $this->get_menu_children($current_menu['menu']);
        }

        /* Take care of nodes with endpoints */
        else if ( $endpoint ) {
            $menu_text = $this->call_endpoint($endpoint);
        }

        /* Current node is a text node */
        else {
            $menu_text = $this->get_text_menu_text($current_menu['menu']);
        }
        /* Add End to string To End the session on the user's phone
         ---------------
         */

        if( !$menu_text ) {

           /* We have exhausted text menus ... someone load the next thing
            * At this point you should end the session with a session end
            * message.
            */
            $menu_text = $this->default_session_end_message;
        }
        return $menu_text;

    }

    /* Prints the string to be displayed on the user's phone */
    public function print_ussd_string () {
        print $this->ussd_string_text;
    }

    /* Menus are cached with the key defined as follows:
     * service_code[*shortcut]
     * Note: The section in square brackets is optional.
     */
    public function set_menu () {
        if ($this->shortcut) {
            $key = $this->service_code . "#" . $this->shortcut;
        }
        else {
            $key = $this->service_code;
        }
        $cache_data = $this->cache->retrieve_from_cache($key);

        /* The Cache'd menu */
        $menu = json_decode($cache_data['results']);

        /* If the menu was not found in cache fetch it from the DB
         * and cache it.
         */
        if (!$menu) {
            $menu = $this->db->get_menu($this->service_code, $this->shortcut);
            /* Cache menu for a day */
            $this->cache->save_to_cache($key, json_encode($menu), 60*60*24);
        }
        $this->menu = $menu;
    }

   //NOTE: Add prev_text_menu_node to cache <= Done
   //Prev node is the index of the previous menu displayed
    private function get_text_menu_text($menu){
        $prev_text_menu_node = isset($this->session_data['prev_text_menu_node']) ?
                    /* True */
                    $this->session_data['prev_text_menu_node'] :
                    /* False */
                    false;  
        /* Check if there was a previous node */
        if ( $prev_text_menu_node ) {
            /* Check if we're on the last text node */
            if ( ( $prev_text_menu_node + 1 ) == count( $menu ) ) {
                $text_menu = false;
            }
            /* There are still some nodes to go. */
            else {
                $text_menu = $menu[++$prev_text_menu_node]['item_name'];
            }
            $cache_value = $prev_text_menu_node;
        }
        /* Return the first node */
        else {
            $text_menu = $menu[0]['item_name'];    
            $cache_value = 0;
        }
        $this->add_to_cache_data('prev_text_menu_node', $cache_value);
        return $text_menu;
    }
    
    private function persist_cache_data () {
        $key = $this->service_code . '#' . $this->msisdn;
        $data = [ $key => $this->cache_data ];
        $this->cache->save_to_cache($key, json_encode($data), 200);
    }

    /* Appends the details supplied to the data to be cache'd */
    private function add_to_cache_data ( $key, $value ) {
        $this->cache_data = array_merge( 
                    $this->cache_data,
                    [ $key => $value ]
                );
    }


    /* Returns an array with the menu at the current level */
    private function get_current_menu ($node){
        $traverse = function ($ca, $v) {
            return $ca[$v];
        };

        $this->set_menu();
        /* Traverse menu */
        $last_menu =  array_reduce(split('\.', $node), $traverse, $this->menu);
        return $last_menu;
    }

    //NOTE: remember to add last_node in session_data <= Done
    //NOTE: remember to add last_node_type in session_data <= Done
    /* Retuns a string representation of the
     * traversal instructions to be applied
     * to obtain the current menu.
     */
    private function get_current_node() {
        $last_node = isset($this->session_data['last_node']) ?
            $this->session_data['last_node'] : false;
        $last_node_type =  isset($this->session_data['last_node_type']) ?
            $this->session_data['last_node_type'] : false;

        /* This is NOT a fresh session and the
         * node selected is a navigation
         * node. ie the selection is NOT an endpoint
         * Input is decremented since php arrays
         * are zero indexed.
         */
        if ( $last_node && ($last_node_type != 'TEXT')) {
            $current_node = $last_node . '.' . ($this->input - 1);
        }

        /* Session is NOT fresh and
         * selected node is an endpoint
         */
        else if ($last_node) {
            $current_node = $last_node;
        }
        /* Session is fresh */
        else {
            $current_node = '0';
            $this->add_to_cache_data('last_node', 0);
        }
        return $current_node;
        
    }
    
    /* Returns text comprising of the names of the
     * menu item names.
     */
    public function get_menu_children ( $menu ) {
        $index=0;
        $get_menu_text = function($cur, $value) use (&$index) {
            return $cur . ++$index . '. ' . $value['item_name'] . PHP_EOL;
        };

        $menu_text = array_reduce($menu, $get_menu_text);
            
        return $menu_text;
    }
    public function call_endpoint ( $endpoint, $data = false, $headers = false ) {
        if ( $headers ) {
            $request = new Url($endpoint, true);
        }
        else {
            $request = new Url($endpoint);
        }
        if ( $data ) {
            $request->setPost($data);
        }
        $request->callUrl();
        return $request;
    }
}


/* Routines
 * --------------------
 * · __construct
 * · initialize_ussd_settings
 * · get_regex
 * · match_content_type
 * · content_type
 * · read_data
 * · validate_ussd_params
 * · valid_params
 * · valid_methods
 * · receive_data
 * · add_to_cache_data
 * · set_ussd_params
 * · get_session_data
 * · get_operator
 * · get_shortcut
 * · get_level
 * · get_input
 * · set_menu
 * · get_ussd_string_from_menu
 * · get_text_menu_text
 * · get_current_menu
 * · get_current_node
 * · get_menu_children
 * --------------------
 */


 /* Test Class */

$ussd = new Request;

print $ussd_string_text . PHP_EOL;
