<?php

require_once('lib/cache.php');

$cache = new MemcacheLib;

$tdata = [
    'name' => 'Reuben',
    'profession' => 'Developer',
    ];
$cache->save_to_cache("test", json_encode($tdata));

$some_value = $cache->retrieve_from_cache("test");

print print_r($some_value, true);
