<?php

if (!defined('ROOT')) define('ROOT', '/var/www/html/ussd_request_parser');
require_once(ROOT . '/.autoload/autoload.php');

$data = [
                'menu_id' => 45,
                'endpoint' => 'http://127.0.0.1:2600/test_endpoint.php',
                'ussd_string' => '4*3*2',
                'operator' => 'safaricom',
                'msisdn' => 254712171204,
        ];
            
$ep = new Endpoint($data);

print $ep->endpoint_result . PHP_EOL;
